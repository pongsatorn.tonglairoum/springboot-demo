package com.example.demo;

import java.util.Optional;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {
    private final PersonRepo personRepo;
    private final NameCheckService nameCheckService;

    @Override
    public CreatePersonResponse createPerson(CreatePersonRequest request) {

        // validate if firstName exists
        Optional<Person> exists = personRepo.findByFirstName(request.getFirstName());

        if (exists.isPresent()) {
            throw new AlreadyExistException();
        }

        // check name
        boolean passed = nameCheckService.check(request.getFirstName());

        if (!passed) {
            throw new NameNotAllowedException();
        }

        // save to db
        var person = Person.builder().firstName(request.getFirstName()).lastName(request.getLastName()).build();
        person = personRepo.save(person);

        // create response
        var response = new CreatePersonResponse();
        response.setId(person.getId());
        response.setSuccess(true);
        return response;
    }

}
