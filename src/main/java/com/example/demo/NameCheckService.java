package com.example.demo;

public interface NameCheckService {

    boolean check(String firstName);

}
