package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class NameCheckServiceImpl implements NameCheckService {
    @Value("${namecheck.url}")
    private String url;

    private final RestTemplate restTemplate;

    @Override
    public boolean check(String name) {
        log.info("calling api {} with name {}", url, name);
        // prepare request
        HttpEntity<NameCheckRequest> request = new HttpEntity<>(NameCheckRequest.builder().name(name).build());

        // post
        NameCheckResponse response = restTemplate.postForObject(url, request, NameCheckResponse.class);
        return response.getOk();
    }

}
