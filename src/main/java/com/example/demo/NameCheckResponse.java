package com.example.demo;

import lombok.Data;

@Data
public class NameCheckResponse {
    private Boolean ok;
}
