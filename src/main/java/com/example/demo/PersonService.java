package com.example.demo;

public interface PersonService {

    CreatePersonResponse createPerson(CreatePersonRequest request);

}
