package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class PersonServiceImplTests {
    private PersonService personService;
    private PersonRepo personRepo;
    private NameCheckService nameCheckService;

    @BeforeEach
    public void before() {
        personRepo = Mockito.mock(PersonRepo.class);
        nameCheckService = Mockito.mock(NameCheckService.class);
        personService = new PersonServiceImpl(personRepo, nameCheckService);
    }

    @Test
    void testCreatePerson() {
        // given
        when(nameCheckService.check(anyString())).thenReturn(true);
        when(personRepo.save(any())).thenReturn(Person.builder().id(100l).build());

        // when
        var result = personService.createPerson(CreatePersonRequest.builder().firstName("Test").build());

        // then
        assertNotNull(result);
        assertEquals(100l, result.getId());
    }
}
